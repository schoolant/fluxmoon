package com.sachittome.fluxmoon;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;

// drawing on the canvas and "running" while the game is started
public class FluxMoonComponent extends Canvas implements Runnable {
	private static final long serialVersionUID = 1L;
	
	// frame width and height
	public static final int HEIGHT = 240;
	public static final int WIDTH = HEIGHT * 16 / 9; // 16:9 aspect res
	public static final int SCALE = 2; // because I want that retro look
	
	// boiler variables
	private boolean running = false;
	
	// image datas
	private BufferedImage image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
	
	public FluxMoonComponent() {
		setMaximumSize(new Dimension(WIDTH*SCALE, HEIGHT*SCALE));
		setMinimumSize(new Dimension(WIDTH*SCALE, HEIGHT*SCALE));
		setPreferredSize(new Dimension(WIDTH*SCALE, HEIGHT*SCALE));
	}
	
	// what happens when the component loads
	public void start() {
		new Thread(this).start();
	}
	
	// what happens when the component stops
	public void stop() {
		running = false;
	}
	
	// what actually updates the tick
	public void run() {
		// only update the game while running
		while(running) {
			
		}
	}
	
	// component updates
	public void tick() {
		
	}
	
	// rendering while the component updates
	public void render() {
		BufferStrategy bs = getBufferStrategy();
		if(bs == null) {
			this.createBufferStrategy(3);
			return;
		}
		
		// set the graphics to the buffer strategy image
		Graphics g = bs.getDrawGraphics();
		// draw the canvas image
		g.drawImage(image, 0, 0, getWidth(), getHeight(), null);
		// dispose gfx and draw buffer
		g.dispose();
		bs.show();
	}
	
	// main game loop, starts the components
	public static void main(String[] args) {
		// create a new game component
		FluxMoonComponent FMC = new FluxMoonComponent();
		
		// create the game window
		JFrame frame = new JFrame("FluxMoon");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new BorderLayout());
		frame.add(FMC);
		frame.pack();
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
}
